from __future__ import print_function
from base64 import b16encode
from classes.ipcammanager import IPCamManager
from classes.gsheet import GSheet

cam = IPCamManager()
gsheet = GSheet('1Cgbt86mJCphkzPzk6q8pf9L6SGqPBeUvv-fkFbqzFUw')
rangeName = 'hourly!A:E'
# cam code
cam.connect('http://192.168.1.2:8080/video')
frame = cam.stream()
avg = cam.exctractAvg(frame)
avgHex = '#%02x%02x%02x' % avg
print(avg)
print(avgHex)
row = [avgHex,avgHex,avgHex,avgHex,avgHex]
# gsheet code
gsheet.append(row, rangeName)
