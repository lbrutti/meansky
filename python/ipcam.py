from __future__ import print_function
import cv2
import urllib
import numpy as np

stream=urllib.urlopen('http://192.168.1.2:8080/video')
bytes=''
idx=0
while (idx<100):
    bytes+=stream.read(1024)
    a = bytes.find('\xff\xd8')
    b = bytes.find('\xff\xd9')
    idx+= 1
    if a!=-1 and b!=-1:
        jpg = bytes[a:b+2]
        bytes= bytes[b+2:]
        i = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8),cv2.CV_LOAD_IMAGE_COLOR)
        b,g,r=cv2.split(i)
        b_avg = int(np.round(np.average(b)))
        g_avg = int(np.round(np.average(g)))
        r_avg = int(np.round(np.average(r)))
        avg = [b_avg, g_avg, r_avg]
        print(avg)
        # cv2.imshow('i',i)
        if cv2.waitKey(1) ==27:
            exit(0)
