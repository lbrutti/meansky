import cv2
import numpy as np

img = cv2.imread('../img/download.jpeg')
b,g,r=cv2.split(img)
b_avg = int(np.round(np.average(b)))
g_avg = int(np.round(np.average(g)))
r_avg = int(np.round(np.average(r)))
avg = [b_avg, g_avg, r_avg]
