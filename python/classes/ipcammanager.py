import cv2
import urllib
import numpy as np

class IPCamManager():
    """docstring for IPCamManager."""
    def __init__(self):
        self.URL=''
        self.bytes=''

    def connect(self,ipCamURL):
        self.URL=ipCamURL


    def stream(self):
        idx=0
        i=0
        stream=urllib.urlopen(self.URL)
        while (idx<100):
            self.bytes+=stream.read(1024)
            a = self.bytes.find('\xff\xd8')
            b = self.bytes.find('\xff\xd9')
            idx+= 1
            if a!=-1 and b!=-1:
                jpg = self.bytes[a:b+2]
                self.bytes= self.bytes[b+2:]
                i = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8),cv2.IMREAD_COLOR)
                return i

    def exctractAvg(self, frame):
        b,g,r=cv2.split(frame)
        b_avg = int(np.round(np.average(b)))
        g_avg = int(np.round(np.average(g)))
        r_avg = int(np.round(np.average(r)))
        avg = (b_avg, g_avg, r_avg)
        return avg
