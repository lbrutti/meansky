from __future__ import print_function
import httplib2
import os

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools

class GSheet():
        """docstring for IPCamManager."""
        def __init__(self, spreadsheetId):
            try:
                import argparse
                self.flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
            except ImportError:
               self.flags = None

            # If modifying these scopes, delete your previously saved credentials
            # at ~/.credentials/sheets.googleapis.com-python-quickstart.json
            self.SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
            self.CLIENT_SECRET_FILE = 'client_secret.json'
            self.APPLICATION_NAME = 'Google Sheets API Python Quickstart'
            self.spreadsheetId = spreadsheetId


        def get_credentials(self):
            """Gets valid user credentials from storage.

            If nothing has been stored, or if the stored credentials are invalid,
            the OAuth2 flow is completed to obtain the new credentials.

            Returns:
                Credentials, the obtained credential.
            """
            home_dir = os.path.expanduser('~')
            credential_dir = os.path.join(home_dir, '.credentials')
            if not os.path.exists(credential_dir):
                os.makedirs(credential_dir)
            credential_path = os.path.join(credential_dir,
                                           'sheets.googleapis.com-python-append.json')

            store = oauth2client.file.Storage(credential_path)
            credentials = store.get()
            if not credentials or credentials.invalid:
                flow = client.flow_from_clientsecrets(self.CLIENT_SECRET_FILE, self.SCOPES)
                flow.user_agent = self.APPLICATION_NAME
                if self.flags:
                    credentials = tools.run_flow(flow, store, self.flags)
                else: # Needed only for compatibility with Python 2.6
                    credentials = tools.run(flow, store)
                print('Storing credentials to ' + credential_path)
            return credentials


        def append(self, row, rangeName):
            credentials = self.get_credentials()
            http = credentials.authorize(httplib2.Http(ca_certs = '/etc/ssl/ca-bundle.pem'))
            discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                            'version=v4')
            service = discovery.build('sheets', 'v4', http=http,
                                      discoveryServiceUrl=discoveryUrl)


            # https://developers.google.com/sheets/guides/values#appending_values
            values = {'values':[row,]}
            result = service.spreadsheets().values().append(
                spreadsheetId=self.spreadsheetId, range=rangeName,
                valueInputOption='RAW',
                body=values).execute()
